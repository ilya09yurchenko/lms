from django.core.exceptions import ValidationError
from django.forms import ModelForm

from group.models import Group


class GroupForm(ModelForm):
    class Meta:
        model = Group
        fields = ("group_name", "email")

    def clean_email(self):
        email = self.cleaned_data["email"]

        if "@yandex" in email.lower():
            raise ValidationError("RU is forbidden")

        return email

