import random

from django.db import models
from faker import Faker


# Create your models here.


class Group(models.Model):
    group_name = models.CharField(max_length=128, null=True)
    email = models.EmailField(max_length=128)

    def __str__(self):
        return f"{self.id} {self.group_name} {self.email}"

    def generate_group(self):
        faker = Faker()

        Group.objects.create(
            group_name=random.randint(100, 500),
            email=faker.email()
        )
