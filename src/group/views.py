from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
# Create your views here.
from webargs import fields
from webargs.djangoparser import use_args

from group.forms import GroupForm
from group.models import Group
from students.utils import format_records


@use_args(
    {
        "group_name": fields.Str(required=False),
        "last_name": fields.Str(required=False),
        "search_text": fields.Str(required=False),
    }
)
def get_group(request, parameters):
    form = """
            <!DOCTYPE html>
            <html>
            <body>

            <h2>HTML Forms</h2>

            <form>
              <label>Group name name:</label><br>
              <input type="text" name="group_name"><br>

              <input type="submit" value="Submit">
            </form> 
    """
    group = Group.objects.all()

    search_fields = ["group_name", "email"]

    for param_name, param_value in parameters.items():
        if param_value:
            if param_name == "search_text":
                or_filter = Q()
                for field in search_fields:
                    or_filter |= Q(**{f"{field}__icontains": param_value})
                group = group.filter(or_filter)
            else:
                group = group.filter(**{param_name: param_value})

    result = format_records(group)

    response = form + result

    return HttpResponse(response)


@csrf_exempt
def create_group(request):
    if request.method == "POST":
        form = GroupForm(request.POST)

        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("group:get_group"))

    elif request.method == "GET":
        form = GroupForm()

    form_html = f"""
        <form method = 'POST'>
              {form.as_p()}
              <input type="submit" value="Create">
        </form> 
    """
    return HttpResponse(form_html)


@csrf_exempt
def update_group(request, pk):
    group = get_object_or_404(Group.objects.all(), pk=pk)

    if request.method == "POST":
        form = GroupForm(request.POST, instance=group)

        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("group:update_group"))

    elif request.method == "GET":
        form = GroupForm(instance=group)

    form_html = f"""
        <form method = 'POST'>
              {form.as_p()}
              <input type="update" value="Update">
        </form> 
    """
    return HttpResponse(form_html)
