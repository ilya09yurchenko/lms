from django.urls import path, include
from group.views import *

app_name = 'group'

urlpatterns = [
    path("", get_group, name="get_group"),
    path("create/", create_group, name="create_group"),
    path("update/<int:pk>/", update_group, name="update_group"),

]
