from django.urls import path, include
from teacher.views import *

app_name = 'teacher'

urlpatterns = [
    path('', get_teacher, name='get_teacher'),
    path("create/", create_teacher, name='create_teacher'),
    path("update/<int:pk>/", update_teacher, name='update_teacher')
]
