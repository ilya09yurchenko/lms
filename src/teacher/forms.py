from django.core.exceptions import ValidationError
from django.forms import ModelForm

from teacher.models import Teacher


class TeacherForm(ModelForm):
    class Meta:
        model = Teacher
        fields = ("first_name", "last_name", "email")

    def clean_email(self):
        email = self.cleaned_data["email"]

        if "@yandex" in email.lower():
            raise ValidationError("Yandex is forbidden")

        return email

    @staticmethod
    def normalize_data(text):
        return text.strip().capitalize()

    def clean_first_name(self):
        return self.normalize_data(self.cleaned_data["first_name"])

    def clean_last_name(self):
        return self.normalize_data(self.cleaned_data["last_name"])

    def clean(self):
        cleaned_data = super().clean()

        first_name = cleaned_data["first_name"]
        last_name = cleaned_data["last_name"]

        if first_name == last_name:
            raise ValidationError("First name equal to last name")

        return cleaned_data
