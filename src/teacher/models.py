from django.db import models
from faker import Faker


# Create your models here.


class Teacher(models.Model):
    first_name = models.CharField(max_length=128, null=True)
    last_name = models.CharField(max_length=128, null=True)
    email = models.EmailField(max_length=128)
    birthdate = models.DateField(null=True)

    def __str__(self):
        return f"{self.id} {self.first_name} {self.last_name} {self.email}"

    def generate_teacher(self):
        faker = Faker()
        Teacher.object.create(
            first_name=faker.first_name(),
            last_name=faker.last_name(),
            email=faker.email(),
            birthdate=faker.date_time_between(start_date="-60y", end_date="-25y"),
        )
