from django.db.models import Q
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from webargs import fields
from webargs.djangoparser import use_args

from students.utils import format_records
from teacher.forms import TeacherForm
from teacher.models import Teacher


# Create your views here.\

@use_args(
    {
        "first_name": fields.Str(required=False),
        "last_name": fields.Str(required=False),
        "search_text": fields.Str(required=False),
    }
)
def get_teacher(request, parameters):
    form = """
            <!DOCTYPE html>
            <html>
            <body>

            <h2>HTML Forms</h2>

            <form>
              <label>First name:</label><br>
              <input type="text" name="fname"><br>

              <label>Last name:</label><br>
              <input type="text" name="lname"><br><br>

              <input type="submit" value="Submit">
            </form> 
    """
    teacher = Teacher.objects.all()

    search_fields = ["first_name", "last_name", "email"]

    for param_name, param_value in parameters.items():
        if param_value:
            if param_name == "search_text":
                or_filter = Q()
                for field in search_fields:
                    or_filter |= Q(**{f"{field}__icontains": param_value})
                teacher = Teacher.filter(or_filter)
            else:
                teacher = Teacher.filter(**{param_name: param_value})

    result = format_records(teacher)

    response = form + result

    return HttpResponse(response)


@csrf_exempt
def create_teacher(request):
    if request.method == "POST":
        form = TeacherForm(request.POST)

        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("teacher:get_teacher"))

    elif request.method == "GET":
        form = TeacherForm()

    form_html = f"""
        <form method = 'POST'>
              {form.as_p()}
              <input type="submit" value="Create">
        </form> 
    """
    return HttpResponse(form_html)


@csrf_exempt
def update_teacher(request, pk):
    teacher = get_object_or_404(Teacher.objects.all(), pk=pk)

    if request.method == "POST":
        form = TeacherForm(request.POST, instance=teacher)

        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("teacher:update_teacher"))

    elif request.method == "GET":
        form = TeacherForm(instance=teacher)

    form_html = f"""
        <form method = 'POST'>
              {form.as_p()}
              <input type="submit" value="Update">
        </form> 
    """
    return HttpResponse(form_html)
