import datetime
import random

from django.core.validators import MinLengthValidator
from django.db import models
from faker import Faker


# Create your models here.


class Student(models.Model):
    first_name = models.CharField(max_length=128, null=True, validators=[MinLengthValidator(2)])
    last_name = models.CharField(max_length=128, null=True, validators=[MinLengthValidator(2)])
    email = models.EmailField(max_length=128)
    birthdate = models.DateField(null=True)
    grade = models.SmallIntegerField(default=0, null=False)

    def __str__(self):
        return f"{self.id} {self.first_name} {self.last_name} {self.email} {self.grade} "

    def age(self):
        return datetime.datetime.now().year - self.birthdate.year

    def generate_student(self, count):
        faker = Faker()
        for _ in range(count):
            Student.objects.create(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                birthdate=faker.date_time_between(start_date="-30y", end_date="-18y"),
                grade=random.randint(0, 100),
            )
