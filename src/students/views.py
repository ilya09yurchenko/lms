import datetime

from django.db.models import Q
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt

from webargs import fields
from webargs.djangoparser import use_kwargs, use_args

from students.forms import StudentForm
from students.models import Student
from students.utils import format_records


# Create your views here.

@use_args(
    {
        "first_name": fields.Str(required=False),
        "last_name": fields.Str(required=False),
        "search_text": fields.Str(required=False),
    }
)
def get_students(request, parameters):
    form = """
            <!DOCTYPE html>
            <html>
            <body>
            
            <h2>HTML Forms</h2>
            
            <form>
              <label>First name:</label><br>
              <input type="text" name="fname"><br>
              
              <label>Last name:</label><br>
              <input type="text" name="lname"><br><br>
              
              <input type="submit" value="Submit">
            </form> 
    """
    students = Student.objects.all()

    search_fields = ["first_name", "last_name", "email"]

    for param_name, param_value in parameters.items():
        if param_value:
            if param_name == "search_text":
                or_filter = Q()
                for field in search_fields:
                    or_filter |= Q(**{f"{field}__icontains": param_value})
                students = students.filter(or_filter)
            else:
                students = students.filter(**{param_name: param_value})

    result = format_records(students)

    response = form + result

    return HttpResponse(response)


@csrf_exempt
def create_student(request):
    if request.method == "POST":
        form = StudentForm(request.POST)

        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("students:get_students"))

    elif request.method == "GET":
        form = StudentForm()

    form_html = f"""
        <form method = 'POST'>
              {form.as_p()}
              <input type="submit" value="Create">
        </form> 
    """
    return HttpResponse(form_html)


@csrf_exempt
def update_student(request, pk):
    student = get_object_or_404(Student.objects.all(), pk=pk)

    if request.method == "POST":
        form = StudentForm(request.POST, instance=student)

        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("students:update_student"))

    elif request.method == "GET":
        form = StudentForm(instance=student)

    form_html = f"""
        <form method = 'POST'>
              {form.as_p()}
              <input type="submit" value="Update">
        </form> 
    """
    return HttpResponse(form_html)


@csrf_exempt
def delete_student(request, pk):
    student = get_object_or_404(Student.objects.all(), pk=pk)

    if request.method == "POST":
        form = StudentForm(request.POST, instance=student)

        if form.is_valid():
            res = Student.objects.get(pk)
            res.delete()
            return HttpResponseRedirect(reverse("students:delete_student"))

    elif request.method == "GET":
        form = StudentForm(instance=student)


    form_html = f"""
            <form method = 'POST'>
                  {form.as_p()}
                  <input type="submit" value="Delete">
            </form> 
    """
    return HttpResponse(form_html)
