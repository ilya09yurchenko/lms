from typing import List


def format_records(records: List) -> str:
    return "<br>".join(str(record) for record in records)
