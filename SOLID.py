class University:
    def __init__(self, attr):
        self.attr = attr

    def studying(self):
        print("Study some disciplines")


class Group(University):
    def __init__(self, walk, speak, learn, attr):
        super().__init__(attr)
        self.walk = walk
        self.speak = speak
        self.learn = learn

    def walk(self):
        print("walk to lessons")

    def speak(self):
        print("speak about something")

    def learn(self):
        print("learn some disciplines")


class Teacher(Group):
    def __init__(self, give_hw, correct_hw, teach, walk, speak, learn):
        super().__init__(walk, speak, learn)
        self.give_hw = give_hw
        self.correct_hw = correct_hw
        self.teach = teach

    def take_hw(self):
        print("Give hw for students")

    def correct_hw(self):
        print("Correct homework")


class Student(Group):
    def __init__(self, pass_hw, pass_ex, walk, speak, learn):
        super().__init__(walk, speak, learn)
        self.pass_hw = pass_hw
        self.pass_ex = pass_ex

    def pass_hw(self):
        print("pass homework to teacher")

    def pass_ex(self):
        print("pass final exam")
