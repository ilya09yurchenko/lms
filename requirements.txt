asgiref==3.5.0
asttokens==2.0.5
backcall==0.2.0
backports.zoneinfo==0.2.1; python_version < '3.9'
black==22.3.0
click==8.1.2
decorator==5.1.1
django-extensions==3.1.5
django==4.0.3
executing==0.8.3
faker==13.3.4
flake8==4.0.1
ipython==8.2.0
jedi==0.18.1
marshmallow==3.15.0
matplotlib-inline==0.1.3
mccabe==0.6.1
mypy-extensions==0.4.3
packaging==21.3
parso==0.8.3
pathspec==0.9.0
pexpect==4.8.0; sys_platform != 'win32'
pickleshare==0.7.5
platformdirs==2.5.1
prompt-toolkit==3.0.29
ptyprocess==0.7.0
pure-eval==0.2.2
pycodestyle==2.8.0
pyflakes==2.4.0
pygments==2.11.2
pyparsing==3.0.8
python-dateutil==2.8.2
six==1.16.0
sqlparse==0.4.2
stack-data==0.2.0
tomli==2.0.1; python_version < '3.11'
traitlets==5.1.1
typing-extensions==4.1.1; python_version < '3.10'
wcwidth==0.2.5
webargs==8.1.0
